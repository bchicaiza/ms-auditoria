import { commonRoutes } from "./common.routes";
import { logRutas } from "./log.routes";

export default [
  ...commonRoutes,
  ...logRutas
];
