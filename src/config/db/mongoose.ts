import mongoose from "mongoose";
import { MongoError } from "mongodb";

mongoose.Promise = global.Promise;

export const connectDB = () => {
  const mongoURI = process.env.MONGODB_URI as string;
  mongoose.connect(
    mongoURI,
    {
      useCreateIndex: true,
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useFindAndModify: false
    },
    (err: MongoError) => {
      if (err) {
        console.warn(err);
        return console.warn("Unable to connect to the db");
      }
      console.log("DB Connected");
    }
  );
};

export { mongoose };
