ARG is_dev=true

#Download base image ubuntu 16.04
FROM node:10.15.3-jessie

#Mainterner
LABEL MAINTAINER="Papagayo Dev"


# install dependencies
WORKDIR /var/www/html/auditoria
COPY package.json package-lock.json* ./
RUN npm cache clean --force && npm install

# set .env
#RUN if [ "$is_dev" = 'true' ]; then; ARG dotenv=.env.dev; fi
#RUN if [ "$is_dev" = 'false' ]; then; ARG dotenv=.env.example; fi
# ARG dotenv=.env.development
# ADD $dotenv .env

# copy app source to image _after_ npm install so that
# application code changes don't bust the docker cache of npm install step
COPY . /var/www/html/auditoria

# TypeScript
# RUN npm run build

# set application PORT and expose docker PORT, 80 is what Elastic Beanstalk expects
ENV PORT 3025
ENV TZ="America/Guayaquil"

EXPOSE 3025

CMD [ "npm", "run", "start:dev" ]
